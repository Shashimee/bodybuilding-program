using UnityEngine;

public class CameraRotator : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Transform _cameraTransform;
    
    [Header("Settings")]
    [SerializeField] private float _speed;

    private void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            _cameraTransform.Rotate(Vector3.up, _speed * Time.deltaTime);
        }   
    }
}
