﻿using UnityEngine;
using UnityEngine.Events;

namespace MapGeneration
{
    [CreateAssetMenu(fileName = "MapGeneratorSettings", menuName = "Generation/MapGeneratorSettings")]
    public class MapGeneratorSettings : ScriptableObject
    {
        [Header("Initialization")]
        public bool GenerateOnAwake;
        
        [Header("Size")]
        public int SizeX;
        public int SizeZ;
        public float SquareSize;

        [Header("Spacing")]
        public float Spacing;

        [Header("Noise")]
        public int MaxHorizontalShift;

        [Header("Gyms")]
        public int GymsCount;
    }
}