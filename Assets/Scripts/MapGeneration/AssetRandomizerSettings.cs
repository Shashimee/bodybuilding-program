﻿using UnityEngine;

namespace MapGeneration
{
    [CreateAssetMenu(fileName = "AssetRandomizerSettings", menuName = "Generation/AssetRandomizerSettings")]
    public class AssetRandomizerSettings : ScriptableObject
    {
        [Header("Size")]
        public float MinSizeMultiplier;
        public float MaxSizeMultiplier;

        [Header("Rotation")]
        public float AngleRotation;
    }
}