﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace MapGeneration
{
    [CreateAssetMenu(fileName = "GenericAssetSpawnerSettings", menuName = "Generation/GenericAssetSpawnerSettings")]
    public class GenericAssetSpawnerSettings : ScriptableObject
    {
        [Header("Tags")]
        public string SpotTag;
        
        [Header("Population")]
        [Tooltip("0 will spawn no cars and 1 will spawn a car on each spot.")]
        [Range(0f,1f)] public float SpawningChance;
        public bool PopulateOnStart;

        [Header("Rotation")]
        public bool AlignRotationWithParent;
    }
}