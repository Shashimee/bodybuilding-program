﻿using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace MapGeneration
{
    [DisallowMultipleComponent]
    public class RandomAssetSpawner : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private AssetList _assetList;

        private void Awake()
        {
            InitializeAsset();
        }

        [Button]
        private void InitializeAsset()
        {
            var asset = _assetList.Assets.PickRandom();
            Instantiate(asset, transform.position, Quaternion.identity, transform);
        }

        [Button]
        private void Clean()
        {
            if (transform.childCount > 0)
            {
                DestroyImmediate(transform.GetChild(0));
            }
        }
    }
}