using System.Collections.Generic;
using BGJ;
using Sirenix.OdinInspector;
using UnityEngine;

namespace MapGeneration
{
    [DisallowMultipleComponent]
    public class MapGenerator : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private MapGeneratorSettings _settings;
        
        [Header("Prefabs")]
        [SerializeField] private GameObject _finalGoal;
        [SerializeField] private GameObject _gym;
        [SerializeField] private PonderatedAssetList _possibleSquares;

        private int[,] _map;
        private int _currentX;
        private int _currentZ;
        
        private enum SquareType
        {
            None,
            Single,
            DoubleHorizontal,
            DoubleVertical,
            Gym,
            Goal
        }

        private class MapPosition
        {
            public int X;
            public int Z;

            public MapPosition(int x, int z)
            {
                X = x;
                Z = z;
            }
        }

        private Transform _transform;
        private MapPosition _goalPosition;
        private List<MapPosition> _gymsPositions = new List<MapPosition>();

        // MONO
        
        private void Awake()
        {
            _transform = transform;

            if (_settings.GenerateOnAwake)
            {
                Generate();
            }
        }
        
        // PUBLIC

        [Button]
        public void Generate()
        {
            InitializeMap();
            CleanChilds();
            GenerateGoalPosition();
            GenerateGymPositions();

            var position = Vector3.zero;
            for (int z = 0; z < _settings.SizeZ; z++)
            {
                position.x = GetXShift();

                for (int x = 0; x < _settings.SizeX; x++)
                {
                    var squareResult = GenerateSquare(position, x, z);
                    switch (squareResult)
                    {
                        case SquareType.Single:
                            break;
                        case SquareType.DoubleHorizontal:
                            x++;
                            position.x += _settings.SquareSize;
                            break;
                        case SquareType.DoubleVertical:
                            break;
                        case SquareType.Gym:
                            break;
                        case SquareType.Goal:
                            break;
                    }
                    position.x += _settings.Spacing;
                }
                position.z += _settings.Spacing;
            }
        }
        
        // PRIVATE

        private void GenerateGoalPosition()
        {
            _goalPosition = new MapPosition(
                Random.Range(0, _settings.SizeX), 
                Random.Range(0, _settings.SizeZ));
        }

        private void GenerateGymPositions()
        {
            while (_gymsPositions.Count < _settings.GymsCount)
            {
                var gymPosition = new MapPosition(
                    Random.Range(0, _settings.SizeX), 
                    Random.Range(0, _settings.SizeZ));
                if (gymPosition.Equals(_goalPosition))
                {
                    continue;
                }
                _gymsPositions.Add(gymPosition);
            }
        }

        private SquareType GenerateSquare(Vector3 position, int x, int z)
        {
            //if (_map[z, x] != 0) return _map[z,x];
            
            GameObject square;
            SquareType squareType = SquareType.None;

            if (IsGoalPosition(x, z))
            {
                square = Instantiate(_finalGoal, position, Quaternion.identity, _transform);
                squareType = SquareType.Goal;
            }
            else if (IsGymPosition(x, z))
            {
                square = Instantiate(_gym, position, Quaternion.identity, _transform);
                squareType = SquareType.Gym;
            }
            else
            {
                var squarePrefab = _possibleSquares.GetRandom();
                square = Instantiate(squarePrefab, position, Quaternion.identity, _transform);
                
                switch (square.tag)
                {
                    case Constants.Tag.SimpleSquare:
                        _map[z,x] = 1;
                        squareType = SquareType.Single;
                        break;
                    case Constants.Tag.DoubleSquareHorizontal:
                        _map[z,x] = 2;
                        _map[z,+1] = 2;
                        squareType = SquareType.DoubleHorizontal;
                        break;
                }
            }
            
            var roadGenerator = square.GetComponentInChildren<RoadGenerator>();

            if (z == (_settings.SizeZ-1))
            {
                roadGenerator.GenerateTop();
            }
            if (x == 0)
            {
                roadGenerator.GenerateLeft();
            }
            roadGenerator.GenerateRight();
            roadGenerator.GenerateBottom();
            return squareType;
        }

        [Button]
        private void CleanChilds()
        {
            for (int i = 0; i < _transform.childCount; i++)
            {
                Destroy(_transform.GetChild(i).gameObject);
            }
        }

        private bool IsGoalPosition(int x, int z)
        {
            return x == _goalPosition.X && z == _goalPosition.Z;
        }

        private bool IsGymPosition(int x, int z)
        {
            foreach (var gymPosition in _gymsPositions)
            {
                if (x == gymPosition.X && z == gymPosition.Z)
                {
                    return true;
                }
            }
            return false;
        }

        private void InitializeMap()
        {
            _map = new int[_settings.SizeZ + 2 * _settings.MaxHorizontalShift, _settings.SizeX];
        }

        private float GetXShift()
        {
            return Random.Range(-_settings.MaxHorizontalShift, _settings.MaxHorizontalShift) * _settings.SquareSize;
        }
    }
}

