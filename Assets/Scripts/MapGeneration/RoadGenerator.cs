﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace MapGeneration
{
    [DisallowMultipleComponent]
    public class RoadGenerator : MonoBehaviour
    {
        [Header("Prefabs")]
        [SerializeField] private GameObject _roadTop;
        [SerializeField] private GameObject _roadRight;
        [SerializeField] private GameObject _roadBottom;
        [SerializeField] private GameObject _roadLeft;
        
        [Header("Positions")]
        [SerializeField] private Transform _top;
        [SerializeField] private Transform _right;
        [SerializeField] private Transform _bottom;
        [SerializeField] private Transform _left;

        [Button]
        public void GenerateTop()
        {
            Instantiate(_roadTop, _top);
        }

        [Button]
        public void GenerateRight()
        {
            Instantiate(_roadRight, _right);
        }

        [Button]
        public void GenerateBottom()
        {
            Instantiate(_roadBottom, _bottom);
        }

        [Button]
        public void GenerateLeft()
        {
            Instantiate(_roadLeft, _left);
        }
    }
}