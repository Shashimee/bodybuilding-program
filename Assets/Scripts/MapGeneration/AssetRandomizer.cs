﻿using UnityEngine;

namespace MapGeneration
{
    [DisallowMultipleComponent]
    public class AssetRandomizer : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private AssetRandomizerSettings _settings;

        private Transform _transform;

        private void Awake()
        {
            _transform = transform;
        }

        private void Start()
        {
            Randomize();
        }

        public void Randomize()
        {
            RandomizeSize();
            RandomizeRotation();
        }

        private void RandomizeSize()
        {
            var sizeModifier = Random.Range(_settings.MinSizeMultiplier, _settings.MaxSizeMultiplier);
            _transform.localScale *= sizeModifier;
        }

        private void RandomizeRotation()
        {
            var newAngle = Random.Range(0, 4) * _settings.AngleRotation;
            transform.RotateAround(transform.position, _transform.up, newAngle);
        }
    }
}