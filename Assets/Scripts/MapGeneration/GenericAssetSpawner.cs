﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Utils;

namespace MapGeneration
{
    [DisallowMultipleComponent]
    public class GenericAssetSpawner : MonoBehaviour
    {
        [Header("Events")]
        public UnityEvent OnPopulated;
        
        [Header("Prefabs")]
        [SerializeField] private AssetList _cars;
        
        [Header("Settings")]
        [SerializeField] private GenericAssetSpawnerSettings _settings;

        private Transform _transform;
        private List<Transform> _spots = new List<Transform>();

        // MONO
        
        private void Awake()
        {
            _transform = transform;
        }

        private void Start()
        {
            if (_settings.PopulateOnStart)
            {
                PopulateSpots();   
            }
        }
        
        // PUBLIC

        [Button]
        public void PopulateSpots()
        {
            CleanChilds();
            GetSpots();
            foreach (var spot in _spots)
            {
                if (ShouldSpawnAsset())
                {
                    SpawnAsset(spot);
                }
            }
            OnPopulated?.Invoke();
        }
        
        // PRIVATE

        private void GetSpots()
        {
            _spots = new List<Transform>();
            var carSpots = GameObject.FindGameObjectsWithTag(_settings.SpotTag);
            foreach (var carSpot in carSpots)
            {
                _spots.Add(carSpot.transform);
            }
        }
        
        private bool ShouldSpawnAsset()
        {
            return Random.Range(0f, 1f) <= _settings.SpawningChance;
        }

        private void SpawnAsset(Transform spot)
        {
            Instantiate(_cars.Assets.PickRandom(), spot.position, spot.rotation, _transform);
        }
        
        [Button]
        private void CleanChilds()
        {
            for (int i = 0; i < _transform.childCount; i++)
            {
                Destroy(_transform.GetChild(i).gameObject);
            }
        }
    }
}