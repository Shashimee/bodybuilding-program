﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MapGeneration
{
    [System.Serializable]
    public class PonderatedAsset
    {
        public GameObject Asset;
        public float Weight;
    }
    
    [CreateAssetMenu(fileName = "PonderatedAssetList", menuName = "Generation/PonderatedAssetList")]
    public class PonderatedAssetList : ScriptableObject
    {
        public List<PonderatedAsset> Assets;

        public GameObject GetRandom()
        {
            var count = 0f;
            var randomWeight = Random.Range(0f, GetTotalWeight());
            foreach (var ponderatedAsset in Assets)
            {
                count += ponderatedAsset.Weight;
                if (randomWeight <= count)
                {
                    return ponderatedAsset.Asset;
                }
            }
            return Assets[Assets.Count].Asset;
        }

        private float GetTotalWeight()
        {
            return Assets.Sum(x => x.Weight);
        }
    }
}