﻿using System.Collections.Generic;
using UnityEngine;

namespace MapGeneration
{
    [CreateAssetMenu(fileName = "AssetList", menuName = "Generation/AssetList")]
    public class AssetList : ScriptableObject
    {
        public List<GameObject> Assets;
    }
}