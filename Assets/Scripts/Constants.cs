using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BGJ
{
    public static class Constants
    {
        public static class Tag 
        {
            public const string Collectable = "Collectable";
            public const string MainCharacter = "MainCharacter";
            public const string SimpleSquare = "SimpleSquare";
            public const string DoubleSquareHorizontal = "DoubleSquareHorizontal";
            public const string DoubleSquareVertical = "DoubleSquareVertical";
            public const string BodybuilderSpawn = "BodybuilderSpawn";
        }
}
}
